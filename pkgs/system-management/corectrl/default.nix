{ stdenv
, fetchgit
, qtbase
, cmake
, botan2
, procps
, hwdata
, karchive
, pkgconfig
, extra-cmake-modules
, qtcharts
, qttools
, kauth
, libdrm
, qtdeclarative
, qtquickcontrols2
, wrapQtAppsHook 
}:


stdenv.mkDerivation rec {
	pname = "CoreCtrl";
	version = "1.0.8";

	src = fetchgit {
		url = "https://gitlab.com/corectrl/corectrl";
		rev = "d013dcba7609e8e9093fc8c70df43c91236d6d3d";
		sha256 = "1xg46a6a03yc6fqfz0k1d3mfydivimqwnvj50cf39rjaqhhgw3yv";
	};

	nativeBuildInputs = [
		cmake
		pkgconfig
		wrapQtAppsHook
	];

	buildInputs = [
		botan2
		karchive
		procps
		hwdata
		extra-cmake-modules
		qtcharts
		qttools
		kauth
		libdrm
		qtdeclarative
		qtquickcontrols2
	];

	cmakeFlags = [
		"-DCMAKE_BUILD_TYPE=Release"
		"-DBUILD_TESTING=OFF"
	];

	meta = with stdenv.lib; {
      description = "Corectrl is a program to control CPUs/AMD GPUs";
      license = "liscenses.gpl3";
      platforms = platforms.linux;
      maintainers = with maintainers; [ cidkid ];
	};
}
