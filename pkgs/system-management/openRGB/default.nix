{ stdenv
, fetchgit
, qmake
, pkgconfig
, libusb
, hidapi
, qtbase
, wrapQtAppsHook 
, fetchurl
}:

stdenv.mkDerivation rec {
  pname = "openRGB";
  version = "2.0";

  src = fetchgit {
    fetchSubmodules = true;
    url = "https://gitlab.com/CalcProgrammer1/OpenRGB.git";
    rev = "13414ec9b84c299631e5100744f2b83923cba3c8";
    sha256 = "0b1mkp4ca4gdzk020kp6dkd3i9a13h4ikrn3417zscsvv5y9kv0s";
  };

  openrgbrules = fetchurl {
	url = "https://gitlab.com/CalcProgrammer1/OpenRGB/-/raw/master/99-openrgb.rules";
	sha256 = "0y6cx995zkrpxyb39bhlbwh5vn7q47f5zfln27cmyawp0cjf2211";
  };

    nativeBuildInputs = [
      pkgconfig
      qmake
      wrapQtAppsHook
	];

    buildInputs = [
      libusb
      hidapi
    ];

    installPhase = ''
      install -Dm744 ./OpenRGB $out/bin/OpenRGB
      mkdir -p $out/lib/udev/rules.d/
      cp $openrgbrules $out/lib/udev/rules.d/
    '';

    qtWrapperArgs = [ ''--prefix PATH : $out/bin/OpenRGB'' ];

    meta = with stdenv.lib; {
      description = "OpenRGB is a management tool for RGB";
      license = "liscenses.gpl2";
      platforms = platforms.linux;
      maintainers = with maintainers; [ cidkid ];
    };
}
