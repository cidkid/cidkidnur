{ stdenv
, fetchgit 
}:

stdenv.mkDerivation rec {
	pname = "material-shell";
	version = "1";

	src = fetchgit {
		url = "https://github.com/PapyElGringo/material-shell.git";
		rev = "d9fa515804622a48b7e0338af64d88c7b4cc0328";
		sha256 = "0n4gvvkx2xr01z390ypbd936j3lizzzpysx7nwfzw93km4js1h4g";
	};

	dontBuild = true;
	uuid = "material-shell@papyelgringo";
	installPhase = ''
		mkdir -p $out/share/gnome-shell/extensions/${uuid}
		cp -r . $out/share/gnome-shell/extensions/${uuid}
	'';

	meta = with stdenv.lib; {
      description = "Material-shell extensions for GNOME 3.36+";
      license = "liscenses.gpl3";
      platforms = platforms.linux;
      maintainers = with maintainers; [ cidkid ];
  };
}
