
{ stdenv, fetchFromGitLab, glib, gettext, substituteAll, gnome-menus }:

stdenv.mkDerivation rec {
  pname = "arc-menu-gnome";
  version = "45";

  src = fetchFromGitLab {
    owner = "arcmenu-team";
    repo = "Arc-Menu";
    rev = "v${version}-Stable";
    sha256 = "0a6n0zyzhsxydz42y89i4ra58cpjx8gnrr5hz2c8y1bp1nrw7rs8";
  };

  patches = [
	(substituteAll {
		src = ./fix_gmenu.patch;
		gmenu_path = "${gnome-menus}/lib/girepository-1.0";
	})
  ];

  buildInputs = [
    glib gettext
  ];

  makeFlags = [ "INSTALLBASE=${placeholder "out"}/share/gnome-shell/extensions" ];

  meta = with stdenv.lib; {
    description = "Gnome shell extension designed to replace the standard menu found in Gnome 3";
    license = licenses.gpl2Plus;
    maintainers = with maintainers; [ dkabot cidkid ];
    homepage = "https://gitlab.com/LinxGem33/Arc-Menu";
  };
}
