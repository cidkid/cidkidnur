{ stdenv
, fetchFromGitHub
, nodePackages
, glib 
}:

stdenv.mkDerivation rec {
	pname = "pop-os-shell";
	version = "0";

	src = fetchFromGitHub {
		owner = "pop-os";
		repo = "shell";
		rev = "16b4c33a8542969f19471b13b90764575a45ccf9";
		sha256 = "0q0wiid5dib55196njm9v5krn6jxfsj8009gqiia9b2l1h8cx0g5";
	};

    buildInputs = [
		nodePackages.typescript
		glib
	];

	uuid = "pop-shell@system76.com";

	makeFlags = [ "INSTALLBASE=$(out)/share/gnome-shell/extensions" ];

	meta = with stdenv.lib; {
      description = "Pop-os-shell";
      license = "liscenses.gpl3";
      platforms = platforms.linux;
      maintainers = with maintainers; [ cidkid ];
  };
}
