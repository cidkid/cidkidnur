{ lib
, python3Packages
, lsof 
}:

python3Packages.buildPythonApplication rec {
	pname = "trackma";
	version = "0.8.2";

	src = python3Packages.fetchPypi {
		inherit pname version;
		sha256 = "090cq4xncxlfbryyvaa98mwl7qszykl104gyj1c0qhh7gs380cfi";
	};

	BuildInputs = [ lsof ];
	propagatedBuildInputs = with python3Packages; [ dbus-python pyinotify pyqt5 ];
	
	meta = with stdenv.lib; {
	      description = "trackma";
    	      license = "liscenses.gpl3";
      	      platforms = platforms.linux;
       	      maintainers = with maintainers; [ cidkid ];
	};
}
