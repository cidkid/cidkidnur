{ stdenv
, fetchFromGitHub
, libX11
, vulkan-tools
, glslang
, vulkan-headers
, vulkan-loader
, vulkan-validation-layers
}:

stdenv.mkDerivation rec {
	pname = "vkBasalt";
	version = "0.3.1";

	src = fetchFromGitHub {
		owner = "DadSchoorse";
		repo = "vkBasalt";
		rev = "v${version}";
		sha256 = "1d4hd4fi7bk2nbapiy6wh4fmc8g67qbqjf9pvcmzfgskd3d4wl8v";
	};

	buildInputs = [
		libX11
		vulkan-tools
		glslang
		vulkan-headers
		vulkan-loader
		vulkan-validation-layers
	];

	postInstall = '' 
		ls .
	'';

	meta = with stdenv.lib; {
		description = "VkBasalt";
		liscense = "liscense.zlib";
		platforms = platforms.linux;
		maintainers = with maintainers; [ cidkid ];
	};
}
