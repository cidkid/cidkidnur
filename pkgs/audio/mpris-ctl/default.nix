{  stdenv
, dbus
, fetchFromGitHub
, pkg-config
}:

stdenv.mkDerivation rec {
	pname = "mpris-ctl";
	version = "0.7.1";
	
	src = fetchFromGitHub {
		owner = "mariusor";
		repo = "mpris-ctl";
		rev = "v${version}";
		sha256 = "0131v0r9w51h6sn4cbmqv2hi2s6h0q34rw88fjj36m46wky3y3l9";	
	};

	nativeBuildInputs = [
		pkg-config
	];
	
	buildInputs = [
		dbus
	];

	postInstall = ''
		ls -la .
	'';
	
	meta = with stdenv.lib; {
		description = "mpris-ctl";
		license = "licenses.gpl3";
		platforms = platforms.linux;
		maintainers = with maintainers; [ cidkid ];
	};
}

