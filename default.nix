let
  pkgs = import <nixpkgs> {};
in

self: super: rec {
	srb2kart = super.callPackage ./pkgs/games/srb2kart {};
	liquidctl = super.callPackage ./pkgs/system-management/liquidctl {};
	openRGB = pkgs.libsForQt5.callPackage ./pkgs/system-management/openRGB {};
	looking-glass = super.callPackage ./pkgs/looking-glass-client {};
	trackma = super.callPackage ./pkgs/pythonPackages/trackma {};
	legendary = super.callPackage ./pkgs/pythonPackages/legendary {};
	corectrl = pkgs.libsForQt5.callPackage ./pkgs/system-management/corectrl {};
	material-shell = super.callPackage ./pkgs/gnome/Extensions/material-shell{};
	pop-os-shell = super.callPackage ./pkgs/gnomeExtensions/pop-os-shell{};
        arc-menu-gnome = super.callPackage ./pkgs/gnomeExtensions/arc-menu-gnome{};

	# Gnome 3.37
	#gnome-shell = super.callPackage ./pkgs/gnome-3.37/gnome-shell {};
	#mutter = super.callPackage ./pkgs/gnome-3.37/mutter {};

	# Disabled
	# mpris-ctl = super.callPackage ./pkgs/audio/mpris-ctl {};
	# discord = super.callPackage ./pkgs/discord {};
	# vkBasalt = super.callPackage ./pkgs/games/vkBasalt {};
} 
